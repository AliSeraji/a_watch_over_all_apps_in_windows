﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Drawing.Text;

namespace watch1
{
    class ChangeFont{
        [DllImport("gdi32.dll")]
        private static extern IntPtr AddFontMemResourceEx(IntPtr dgfont, GraphicsUnit cbfont, IntPtr pdv, [In] ref GraphicsUnit pcFonts);

        private FontFamily ff;
        private Font f;


        public FontFamily GetFontFamily() { return ff; }
        public Font GetFont(){return f;}
        

        public void load_font(){
            byte[] font_array = watch1.Properties.Resources.digital_7;
            int dataLength = watch1.Properties.Resources.digital_7.Length;

            IntPtr ptrdata = Marshal.AllocCoTaskMem(dataLength);
            Marshal.Copy(font_array, 0, ptrdata, dataLength);

            GraphicsUnit fonts = 0;
            AddFontMemResourceEx(ptrdata, (GraphicsUnit)font_array.Length, IntPtr.Zero, ref fonts);
            PrivateFontCollection pfc = new PrivateFontCollection();
            pfc.AddMemoryFont(ptrdata, dataLength);

            Marshal.FreeCoTaskMem(ptrdata);

            ff = pfc.Families[0];
            f = new Font(ff, 15f, FontStyle.Regular);

        }
        
        public void alloc_font(Font f1, Control c, float size){
            FontStyle fontstyle = FontStyle.Regular;
            c.Font = new Font(ff, size, fontstyle);
        }
        
    }
}
