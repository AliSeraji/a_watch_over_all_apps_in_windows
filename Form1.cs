﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows;
using System.Runtime.InteropServices;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Reflection;

namespace watch1
{
    public partial class Form1 : Form
    {
        int panelWidth;
        bool hidden;
        ChangeFont chf = new ChangeFont();
        private Font favoriteFont;
        private Color favoriteColor;
        private int favoriteFonSize;
        
        public Form1(){
            InitializeComponent();
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            this.BackColor = Color.Transparent;
            this.BackColor = Color.DarkBlue;
            this.TransparencyKey = Color.DarkBlue;
            hidden = true;
            panelWidth = panel1.Width;
            panel1.Width -= panel1.Width;
        }

        public int newSize{
            set { favoriteFonSize = value; }
        }
        public Font newFont{
            set { favoriteFont = value; }
        }
        public Color newColor{
            set { favoriteColor = value; }
        }
        private void timer_Tick(object sender, EventArgs e){
            HourMinute.Text = DateTime.Now.ToString("HH:mm");
            this.BringToFront();
        }

        private void Form1_Load(object sender, EventArgs e){
            timer.Start();
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.TopMost = true;
            
            chf.load_font();
            chf.alloc_font(chf.GetFont(), this.HourMinute, 30);
            form_appearance(sender, e);
            pictureBox1.Visible = false;
            //button1.Visible = false;                                  //needed for showing a transparent button
        }
        
        private void form_appearance(object sender, EventArgs e){
            default_location(sender, e);
            this.TopMost = true;
            this.SetTopLevel(true);
            HourMinute.BackColor = Color.Transparent;                   //needed

        }

        private void default_location(object sender, EventArgs e){
            int width = Screen.PrimaryScreen.Bounds.Width;
            int height = Screen.PrimaryScreen.Bounds.Height;
            int x = width - this.Width;
            int y = height - this.Height;
            //this.Location = new Point(x, y - 50);
            this.Location = new System.Drawing.Point(x, y - 50);

        }



        /*
         private bool MouseIsOverControl(Button btn){                                   //needed for showing a transparent button
             if (btn.ClientRectangle.Contains(btn.PointToClient(Cursor.Position))){       //needed for showing a transparent button
                 return true;                                                             //needed for showing a transparent button
             }                                                                            //needed for showing a transparent button                                                                                                          
             return false;
         }
        private void Form1_MouseMove_1(object sender, MouseEventArgs e)
        {
            if (MouseIsOverControl(this.button1))                                        //needed for showing a transparent button
                this.button1.Visible = true;                                             //needed for showing a transparent button
            else                                                                         //needed for showing a transparent button
                this.button1.Visible = false;
        }
        */

        private bool MouseIsOverControl(PictureBox pic){
            if (pic.ClientRectangle.Contains(pic.PointToClient(Cursor.Position)))
                return true;
            return false;

        }
        private void Form1_MouseMove_1(object sender, MouseEventArgs e){
            if (MouseIsOverControl(this.pictureBox1))                                        //needed for showing a transparent button
                this.pictureBox1.Visible = true;                                             //needed for showing a transparent button
            else                                                                             //needed for showing a transparent button
                this.pictureBox1.Visible = false;
        }
        private void HourMinute_MouseClick(object sender, MouseEventArgs e){
            var options = new Options(this);
            if (e.Button == MouseButtons.Right){
                if (System.Windows.Forms.Application.OpenForms.OfType<Options>().Count() == 1)
                    System.Windows.Forms.Application.OpenForms.OfType<Options>().First().Close();
                else
                    options.Show();      
            }
        }
        internal void populateColor(){
            HourMinute.ForeColor = favoriteColor;
        }

        internal void populateFont(){
            HourMinute.Font = favoriteFont;
        }

        private void pictureBox1_Click(object sender, EventArgs e){
            timer1.Start();
        }

        
        private void timer1_Tick(object sender, EventArgs e){
            if (hidden){
                panel1.Width+= 10;
                if (panel1.Width >= panelWidth){
                    timer1.Stop();
                    hidden = false;
                    this.Refresh();
                }
            }
            else{
                panel1.Width -= 10;
                if (panel1.Width<=0){
                    timer1.Stop();
                    hidden = true;
                    this.Refresh();

                }
            }
        }

        private void button1_Click(object sender, EventArgs e){
            ChangeFontAndeColor cfc = new ChangeFontAndeColor(this);
            timer1.Stop();
            cfc.Show();
            //this.Hide();
        }
    }
}
