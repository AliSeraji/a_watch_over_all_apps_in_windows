﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace watch1{
    public partial class ChangeFontAndeColor : Form{
        
        Form1 originalForm;
        
        Font myChoosenFont;
        int myChoosenFontSize=20;

        public ChangeFontAndeColor(Form1 incommingForm){
            InitializeComponent();
            this.originalForm = incommingForm;
            
        }

        private void pickColor1_theSelectedColor(object sender, EventArgs e){
            TheChoosenColor.BackColor= pickColor1.Selected_Color;
            
        }

        private void button1_Click(object sender, EventArgs e){
            this.Close();
        }

        private void pictureBox1_Click(object sender, EventArgs e){
            TheChoosenColor.BackColor = pictureBox1.BackColor;
        }

        private void pictureBox2_Click(object sender, EventArgs e){
            TheChoosenColor.BackColor = pictureBox2.BackColor;
            
        }
        private void pictureBox3_Click(object sender, EventArgs e){
            TheChoosenColor.BackColor = pictureBox3.BackColor;
            
        }

        private void pictureBox4_Click(object sender, EventArgs e){
            TheChoosenColor.BackColor = pictureBox4.BackColor;
            
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox5.BackColor;
            
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox6.BackColor;
            
        }

        private void pictureBox9_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox9.BackColor;
            
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox8.BackColor;
            
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox7.BackColor;
            
        }

        private void pictureBox10_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox10.BackColor;
            
        }

        private void pictureBox11_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox11.BackColor;
            
        }

        private void pictureBox12_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox12.BackColor;
            
        }

        private void pictureBox13_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox13.BackColor;
            
        }

        private void pictureBox14_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox14.BackColor;
            
        }

        private void pictureBox15_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox15.BackColor;
            
        }

        private void pictureBox16_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox16.BackColor;
            
        }

        private void pictureBox17_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox17.BackColor;
            
        }

        private void pictureBox18_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox18.BackColor;
            
        }

        private void pictureBox39_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox39.BackColor;
            
        }

        private void pictureBox35_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox35.BackColor;
            
        }

        private void pictureBox31_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox31.BackColor;
            
        }

        private void pictureBox27_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox27.BackColor;
            
        }

        private void pictureBox23_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox23.BackColor;
            
        }

        private void pictureBox19_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox19.BackColor;
            
        }

        private void pictureBox40_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox40.BackColor;
            
        }

        private void pictureBox36_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox36.BackColor;
            
        }

        private void pictureBox32_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox32.BackColor;
            
        }

        private void pictureBox28_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox28.BackColor;
            
        }

        private void pictureBox24_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox24.BackColor;
            
        }

        private void pictureBox20_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox20.BackColor;
            
        }

        private void pictureBox41_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox41.BackColor;
            
        }

        private void pictureBox37_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox37.BackColor;
            
        }

        private void pictureBox33_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox33.BackColor;
            
        }

        private void pictureBox29_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox29.BackColor;
            
        }

        private void pictureBox25_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox25.BackColor;
            
        }

        private void pictureBox21_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox21.BackColor;
            
        }

        private void pictureBox42_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox42.BackColor;
            
        }

        private void pictureBox38_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox38.BackColor;
            
        }

        private void pictureBox34_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox34.BackColor;
            
        }

        private void pictureBox30_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox30.BackColor;
            
        }

        private void pictureBox26_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox26.BackColor;
            
        }

        private void pictureBox22_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox22.BackColor;
            
        }

        private void pictureBox43_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox44.BackColor;
            
        }

        private void pictureBox44_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox44.BackColor;
            
        }

        private void pictureBox45_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox45.BackColor;
            
        }

        private void pictureBox46_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox46.BackColor;
            
        }

        private void pictureBox47_Click(object sender, EventArgs e)
        {
            TheChoosenColor.BackColor = pictureBox47.BackColor;
            
        }

        private void pictureBox48_Click(object sender, EventArgs e){
            TheChoosenColor.BackColor = pictureBox48.BackColor;
            
        }

        private void button3_Click(object sender, EventArgs e){
            originalForm.newColor = TheChoosenColor.BackColor;
            originalForm.newFont = myChoosenFont;
            
            originalForm.populateColor();
            originalForm.populateFont();
            this.Close();
            originalForm.Show();
        }

        private void comboBox1_Click(object sender, EventArgs e){
            
        }

        private void ChangeFontAndeColor_Load(object sender, EventArgs e){
            this.Focus();   
            foreach (FontFamily oneFontFamily in FontFamily.Families){
                fontList.Items.Add(oneFontFamily.Name);    
            }
        }

        private void onSelectingNewFont(object sender, EventArgs e){
            var selectedItem = fontList.SelectedItem.ToString();
            label2.Font = myChoosenFont = new Font(fontList.SelectedItem.ToString(), myChoosenFontSize);
            label2.Text = selectedItem;
            string text = textBox1.Text;
            if(text.Length>0)
                foreach(char s in text){
                    if (!char.IsDigit(s)){ textBox1.Clear(); }
                }
            myChoosenFontSize = make_size(text);
            
           
        }

        private int make_size(string txt){
            /*int num = 0;
            int num1 = 0;
            if (txt.Length > 0){
                foreach(char c in txt){
                    num +=Convert.ToInt32(c)-'0';
                    if(num!=num1)
                        num *= 10;
                    num1 = num;
                }
            }
            else{return 30;}*/
            return 0;
        }

        private void keep_focus(object sender, EventArgs e){
            if (this.ClientRectangle.Contains(this.PointToClient(Cursor.Position))){
                this.Focus();
                this.TopMost = true;
                this.SetTopLevel(true);
                this.BringToFront();

            }
        }

        private void ChangeFontAndeColor_MouseMove(object sender, MouseEventArgs e){
            
            if (this.ClientRectangle.Contains(this.PointToClient(Cursor.Position))){
                this.Focus();
                this.TopMost = true;
                this.SetTopLevel(true);
                this.BringToFront();
            }
        }

        
    }
}
