﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace watch1
{
    
    public partial class Options : Form{
        Form1 originalForm;
        public Options(Form1 inComingForm){
            originalForm = inComingForm;
            InitializeComponent();
        }
       
       
        private void Options_Load(object sender, EventArgs e){
            //this.TopLevel = true;
            //this.TopMost = true;
            this.BringToFront();
        }

        private void button1_Click(object sender, EventArgs e){
            if (colorDlg.ShowDialog() == DialogResult.OK){
                originalForm.newColor = colorDlg.Color;
                originalForm.populateColor();   
            }
        }

        private void button2_Click(object sender, EventArgs e){
            if (fontDlg.ShowDialog() == DialogResult.OK){
                originalForm.newFont = fontDlg.Font;
                originalForm.populateFont();
            }
        }

        private void button3_Click(object sender, EventArgs e){
            this.Hide();
        }
    }
}
