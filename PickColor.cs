﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace watch1{
    class PickColor: Panel{
        private Bitmap _canvas;
        private Graphics _graphicsBuffer;
        private LinearGradientBrush _lgbSpectrum , _lgbFade;
        private Color selectedColor;
        public event EventHandler theSelectedColor;

        private void OnColorSelected(){
            if (theSelectedColor != null){
                theSelectedColor(this, EventArgs.Empty);
            }
        }


        public PickColor(){
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.UserPaint | 
                ControlStyles.AllPaintingInWmPaint |ControlStyles.ResizeRedraw,true);
            UpdateBackBuffer();
            UpdateGradients();
        }

        
        private void UpdateBackBuffer(){
            if (!this.Width.Equals(0)){
                _canvas = new Bitmap(this.ClientSize.Width, this.ClientSize.Height);
                _graphicsBuffer = Graphics.FromImage(_canvas);
            }
        }

        private void UpdateGradients(){
            //Update Spectrum

            _lgbSpectrum =new LinearGradientBrush(Point.Empty,new Point(this.ClientSize.Width,0),Color.Wheat,Color.Wheat);
            ColorBlend spectrumBlend = new ColorBlend();
            spectrumBlend.Colors = new Color[]{Color.Violet,Color.Red,Color.Orange,Color.Yellow,Color.Green,Color.Blue,Color.Indigo,Color.Gray};
            spectrumBlend.Positions = new float[] {0,1/8F,2/8F,3/8F,4/8F,5/8F,6/8F,1 };
            _lgbSpectrum.InterpolationColors = spectrumBlend;

            //Update Overlay
            _lgbFade = new LinearGradientBrush(this.ClientRectangle,Color.White,Color.White,90F);
            ColorBlend fadeBlend = new ColorBlend();
            fadeBlend.Colors = new Color[] { Color.White, Color.Transparent, Color.Black };
            fadeBlend.Positions = new float[] { 0, 0.5F, 1 };
            _lgbFade.InterpolationColors = fadeBlend;

        }
        protected override void OnSizeChanged(EventArgs e){
            base.OnSizeChanged(e);
            UpdateBackBuffer();
            UpdateGradients();
        }
        protected override void OnPaint(PaintEventArgs e){
            base.OnPaint(e);
            _graphicsBuffer.FillRectangle(_lgbSpectrum,this.ClientRectangle);
            _graphicsBuffer.FillRectangle(_lgbFade, this.ClientRectangle);
            e.Graphics.DrawImageUnscaled(_canvas, Point.Empty);
        }
        protected override void OnMouseClick(MouseEventArgs e){
            base.OnMouseClick(e);
            Point pos = this.PointToClient(Cursor.Position);
            selectedColor = _canvas.GetPixel(pos.X,pos.Y);
            OnColorSelected();
        }
        public Color Selected_Color{
            get { return selectedColor; }
        }
    }
}
